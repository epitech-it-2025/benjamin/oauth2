import axios from 'axios';

export async function callApi(method, endpoint, data) {
  const url = process.env.API_URL + endpoint;

  let headers= {
    'Content-Type': 'application/json',
  }

  try {
    let response = {}
    if(method == 'get') {
      response = await axios[method](
        url,
        {headers: headers}
      )
    } else {
      response = await axios[method](
        url,
        data,
        {headers: headers}
      )
    }

    return {
      data: response.data,
      status: response.status,
      headers: response.headers,
      statusText: null,
      onSuccess: true,
    }
  } catch (error) {
    console.log(Object.keys(error));
    console.log(error.code);
    console.log(error.message);
    console.log("Request failed: " + endpoint);
    console.error(error);

    return  {
      data: null,
      status: error.code,
      statusText: error.message,
      onSuccess: false
    }
  }
}

export async function registerUser(data) {
  let response = await callApi('post', `/register`, data);
  if (response.onSuccess) {
    return response.data;
  } else {
    return [];
  }
}
