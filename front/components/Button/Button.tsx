import { string } from 'prop-types';
import styles from './Button.module.scss';

function Button({ children }: { children: React.ReactElement | string })
{
    return (
        <button className={styles.custom}>{children}</button>
    )
}

export default Button;