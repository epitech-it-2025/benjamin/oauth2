import styles from '../styles/Login.module.scss'
import { Button, SignInOauth2 } from '../components'
import type { NextPage } from 'next'
import { useSession, signIn, signOut } from "next-auth/react"
import { useEffect, useState } from 'react'
import { registerUser } from '../scripts/Utils'

const Login: NextPage = () => {
  const { data } = useSession()
  const { data: session } = useSession()
  const [checkEnded, setCheckEnded] = useState(false);

  useEffect(() => {
    if (session === undefined) {
      return;
    } else if (session?.user !== undefined) {
      registerUser({
        name: session?.user?.name,
        image: session?.user?.image,
        email: session?.user?.email,
        expires: session?.expires,
      }).then((response) => {
        console.log("data response", response);
      })
    }
    setCheckEnded(true);
  }, [session])

  return (
    <div className={styles.background}>
      <div className={styles.container}>
        <nav className={styles.navbar}>
          <div className={styles.navbarLinks}>
            <a href="#">Home</a>
            <a href="#">About</a>
            <a href="#">Contact</a>
          </div>
          <div className={styles.signUp}>
            {session && session.user && (
              <button onClick={() => signOut()}>Sign out</button>
            )}
          </div>
        </nav>
        {checkEnded && session && session.user ? (
          <div className={styles.logged}>
            <p>You are logged as</p>
            {data?.user?.name && (
              <img src={data.user.image as string} referrerPolicy="no-referrer"/>
            )}
            <p>{data?.user?.name}</p>
            <p>{data?.user?.email}</p>
          </div>
        ) : checkEnded ? (
          <div className={styles.popup}>
            <form className={styles.forms}>
              <label>
                Username:
                <input type={'text'}/>
              </label>
              <label>
                Password:
                <input type={'password'}/>
              </label>
              <Button>{'Login'}</Button>
            </form>
            <SignInOauth2/>
          </div>
        ) : (
          <h2>Loading...</h2>
        )}
      </div>
    </div>
  )
}

export default Login