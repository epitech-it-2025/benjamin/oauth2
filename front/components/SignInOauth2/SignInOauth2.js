import { getProviders, signIn } from "next-auth/react";
import { useEffect, useState } from "react";
import styles from './SignInOauth2.module.scss'

export default function SignInOauth2() {
    const [providers, setProviders] = useState(null);

    useEffect(() => {
        async function getServerSideProps() {
            let response = await getProviders()
            setProviders(response);
        }
        getServerSideProps();
    }, []);

    function getIcon(id) {
        return "/icons/companies/" + id + ".png";
    }

    return (
        <>
            {providers && Object.values(providers).map((provider) => (
                <div key={provider.name} className={styles.provider}>
                    <button onClick={() => signIn(provider.id)}>
                        <img src={getIcon(provider.id)}/>
                        <p>Sign in with {provider.name}</p>
                    </button>
                </div>
            ))}
        </>
    )
}