import * as React from 'react';
import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';
import { Button, View, StyleSheet } from 'react-native';

const App = () => {
      const [request, response, promptAsync] = Google.useAuthRequest({
        expoClientId: '18637987833-dcv5e9m33fus4su29df93e69dlavugee.apps.googleusercontent.com',
        iosClientId: '18637987833-dcv5e9m33fus4su29df93e69dlavugee.apps.googleusercontent.com',
        androidClientId: '18637987833-dcv5e9m33fus4su29df93e69dlavugee.apps.googleusercontent.com',
        webClientId: '18637987833-dcv5e9m33fus4su29df93e69dlavugee.apps.googleusercontent.com',
      });
    
    React.useEffect(() => {
        if (response?.type === 'success') {
          const { authentication } = response;
        }
      }, [response]);
      
      return (
        <View style={styles.loginButtonSection}>
          <Button
            style={styles.loginButton}
            disabled={!request}
            title="Login"
            onPress={() => {
              console.log(promptAsync());
            }}
          />
        </View>
      );
}

const styles = StyleSheet.create({
  loginButtonSection: {
     width: '100%',
     height: '30%',
     height: 500,
     justifyContent: 'center',
     alignItems: 'center'
  },

  loginButton: {
    padding: 50,
    backgroundColor: 'blue',
    color: 'white'
  }
});

// use the client to make the auth request and receive the authState
export default App;