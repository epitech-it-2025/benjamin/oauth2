var express = require("express");
var fs = require("fs/promises");
var _ = require("lodash");
var f = require("fs");
var cors = require("cors");

const app = express();

var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200,
  methods: ["GET", "POST"],
};

app.use(cors(corsOptions));

app.use(express.json());

app.get("/outfit", (req, res) => {
  const tops = ["Black", "White", "Orange", "Navy"];
  const jeans = ["Green", "Dark Gray", "Bleu", "red"];
  const shoes = ["White", "Grey", "Black"];

  res.json({
    top: _.sample(tops),
    jeans: _.sample(jeans),
    shoes: _.sample(shoes),
  });
});

app.post("/register", async (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const image = req.body.image;
  const expires = req.body.expires;

  if (
    name == undefined ||
    email == undefined ||
    image == undefined ||
    expires == undefined
  ) {
    return res.status(400).send("Bad Request");
  }

  let data = {
    name: name,
    email: email,
    image: image,
    expires: expires,
  };

  await fs.mkdir("data/users", { recursive: true });

  if (f.existsSync(`data/users/${email}.json`)) {
    console.log("Users already Exist");
    return res.status(200).send(email);
  }
  await fs.writeFile(`data/users/${email}.json`, JSON.stringify(data));
  console.log("New User created with email : " + email);

  return res.status(200).send(email);
});

app.get("/comments/:id", async (req, res) => {
  const id = req.params.id;
  var content;

  try {
    console.log("we try to open" + `data/comments/${id}.json`);

    content = await fs.readFile(`data/comments/${id}.json`, (err, data) => {
      if (err) throw err;
      let jsonData = JSON.stringify(data);
      console.log("we have : " + jsonData);
      return jsonData;
    });
  } catch (err) {
    return res.status(404).send("No data found");
  }

  res.status(200).send(content);
});

app.listen(9999, () => console.log("APi server is running ..."));
